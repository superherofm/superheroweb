
    <div id='modal-login'>
        <a style='float: right;' class="close b-close"></a>
        <div class="bodyView">
            <div class="layoutView contentView" style="height: ;">
                <div class="layoutSubview" style="z-index: 100; height: ; transform: translateY(0px); opacity: 1; transition: none; -webkit-transition: none;">
                    <header>
                        <div class='content' style='min-width:280px; width: 100%; padding: 15px; text-align: center; color: #000; background: #eee; border-radius: 25px; border-bottom: 2px solid #ccc;'>
                            <strong>SuperHeroFM.com user settings</strong>
                            <br/>
                            <i>The following is your SuperHeroFM.com username and password.  Write it down.</i>
                        </div>
                        <div class="content" style='background: #eee; text-align: center;'>
                            <div class="image">
                                <img src="72x72.png">
                            </div>
                        </div>
                    </header>


                    <div class='container col-sm-6 col-sm-6' style='position: relative; top: 0px; padding: 15px; color: #000; width: 95%; font-size: .9em;'>
                        <form id='superherofm_user_form' method='post' onsubmit='return false;'>


                            <div class="input-group">
                                <span class="input-group-addon">Username</span>
                                <input type="text" class="form-control" id='superherofm_user_form_username' value="">
                            </div>

                            <div class="input-group" style='margin-top: 10px;'>
                                <span class="input-group-addon">Password</span>
                                <input type="text" class="form-control" id='superherofm_user_form_password' value="">
                            </div>

                        </form>

                    </div>

                </div>



            </div>
        </div>

    </div>
	
	<div id='modal-enterprise-access' class='modal-access' style='min-height: 370px;'>
        <a style='float: right;' class="close b-close"></a>
        <div class="bodyView">
            <div class="layoutView contentView" style="height: ;">
                <div class="layoutSubview" style="z-index: 100; height: ; transform: translateY(0px); opacity: 1; transition: none; -webkit-transition: none;">
                    <header>
                        <div class='content' style='min-width:280px; width: 100%; padding: 15px; text-align: center; color: #000; background: #eee; border-radius: 25px; border-bottom: 2px solid #ccc;'>
                            <strong>Enterprise</strong>
                            <br/>
                            <i>Make a secure payment with Paypal</i>
                        </div>
                        <div class="content" style='background: #eee; text-align: center;'>
                            <div class="image">
                                <img src="72x72.png">
                            </div>
                        </div>
                    </header>


                    <div class='container col-sm-6 col-sm-6' style='position: relative; top: 0px; padding: 15px; color: #000; width: 95%; font-size: .9em; min-height: 210px;'>
                        
                       <div id='toggle-enterprise-paypal'>
                            
                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

                                <!-- Identify your business so that you can collect the payments. -->
                                <input type="hidden" name="business" value='5f442df38736b4f8f4a94a02edc44d35afa712d30ff02c5466e12339ee8e4e2f'>

                                <!-- Specify a Subscribe button. -->
                                <input type="hidden" name="cmd" value="_xclick-subscriptions">
                                <!-- Identify the subscription. -->
                                <input type="hidden" name="item_name" value="Enterprise">
                                <input type="hidden" name="item_number" value="EnterpriseBig">

                                <!-- Set the terms of the regular subscription. -->
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="a3" value="360000">
                                <input type="hidden" name="p3" value="1">
                                <input type="hidden" name="t3" value="Y">
                                
                                <!-- Set recurring payments until canceled. -->
                                <input type="hidden" name="src" value="1">

                                <!-- Display the payment button. -->
                                <input type="image" name="submit"
                                src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribe_LG.gif"
                                alt="Subscribe">
                                <img alt="" width="1" height="1"
                                src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
                            </form>	
                            
                            * Above the auspices of Defense to Gold, Defense Golden Ticket, etc.  Paid by the year :)
                            
                        </div>
                                            

                    </div>

                </div>



            </div>
        </div>

    </div>
	
    <div id='modal-1weeklyproxy-access'>
        <a style='float: right;' class="close b-close"></a>
        <div class="bodyView">
            <div class="layoutView contentView" style="height: ;">
                <div class="layoutSubview" style="z-index: 100; height: ; transform: translateY(0px); opacity: 1; transition: none; -webkit-transition: none;">
                    <header>
                        <div class='content' style='min-width:280px; width: 100%; padding: 15px; text-align: center; color: #000; background: #eee; border-radius: 25px; border-bottom: 2px solid #ccc;'>
                            <strong>Proxy Settings</strong>
                            <br/>
                            <i>Copy these settings to your Windows Phone WLAN settings area.</i>
                        </div>
                        <div class="content" style='background: #eee; text-align: center;'>
                            <div class="image">
                                <img src="72x72.png">
                            </div>
                        </div>
                    </header>


                    <div class='container col-sm-6 col-sm-6' style='position: relative; top: 0px; padding: 15px; color: #000; width: 95%; font-size: .9em;'>
                        <form id='superherofm_stripe_proxy_access' method='post' onsubmit='return false;'>

                            <div class="input-group">
                                <span class="input-group-addon">Hostname</span>
                                <select class="form-control">
                                    <option>superherofm.com</option>
                                </select>

                            </div>

                            <div class="input-group" style='margin-top: 10px;'>
                                <span class="input-group-addon">Port</span>
                                <input type="text" class="form-control" value='3128'>
                            </div>

                        </form>

                    </div>

                </div>



            </div>
        </div>

    </div>



    <div id='modal-1weeklyvpn-access'>
        <a style='float: right;' class="close b-close"></a>
        <div class="bodyView">
            <div class="layoutView contentView" style="height: ;">
                <div class="layoutSubview" style="z-index: 100; height: ; transform: translateY(0px); opacity: 1; transition: none; -webkit-transition: none;">
                    <header>
                        <div class='content' style='min-width:280px; width: 100%; padding: 15px; text-align: center; color: #000; background: #eee; border-radius: 25px; border-bottom: 2px solid #ccc;'>
                            <strong>Windows Phone VPN Settings</strong>
                            <br/>
                            <i>Copy these settings to your Windows Phone VPN settings area.</i>
                        </div>
                        <div class="content" style='background: #eee; text-align: center;'>
                            <div class="image">
                                <img src="72x72.png">
                            </div>
                        </div>
                    </header>


                    <div class='container col-sm-6 col-sm-6' style='position: relative; top: 0px; padding: 15px; color: #000; width: 95%; font-size: .9em;'>
                        <form id='superherofm_stripe_vpn_access' method='post' onsubmit='return false;'>

                            <div class="input-group">
                                <span class="input-group-addon"><i class='glyphicon glyphicon-chevron-down'></i></span>
                                <span class='form-control' style='width: 200px;'><a href='https://superherofm.com/ca.cer' target='_blank'>Download &amp; install certificate.</a></span>
                            </div>

                            <div class="input-group" style='margin-top: 10px;'>
                                <span class="input-group-addon">Hostname</span>
                                <input type="text" class="form-control" value="dallas.superherofm.com">
                            </div>

                            <div class="input-group" style='margin-top: 10px;'>
                                <span class="input-group-addon">Type</span>
                                <input type="text" class="form-control" value="IKEv2">
                            </div>

                            <div class="input-group" style='margin-top: 10px;'>
                                <span class="input-group-addon">Connect via</span>
                                <input type="text" class="form-control" value="User name + password">
                            </div>

                            <div class="input-group" style='margin-top: 10px;'>
                                <span class="input-group-addon">Username</span>
                                <input type="text" class="form-control" value="superherofm">
                            </div>

                            <div class="input-group" style='margin-top: 10px;'>
                                <span class="input-group-addon">Password</span>
                                <input type="text" class="form-control" value="superherofm">
                            </div>

                        </form>

                    </div>

                </div>



            </div>
        </div>

    </div>

    <div id='modal-stripe'>
        <a style='float: right;' class="close b-close"></a>
        <div class="bodyView">
            <div class="layoutView contentView" style="height: ;">
                <div class="layoutSubview" style="z-index: 100; height: ; transform: translateY(0px); opacity: 1; transition: none; -webkit-transition: none;">
                    <header>
                        <div class='content' style='min-width:280px; width: 100%; padding: 15px; text-align: center; color: #000; background: #eee; border-radius: 25px; border-bottom: 2px solid #ccc;'>
                            <strong class='modal-pricing'>Subscribe for $1.33/week</strong>
                            <br/>
                            <span class='modal-label'>SuperHeroFM <i>Windows Phone VPN</i></span>
                        </div>
                        <div class="content" style='background: #eee; text-align: center;'>
                            <div class="image">
                                <img src="72x72.png">
                            </div>
                        </div>
                    </header>


                    <div class='container col-sm-6 col-sm-6' style='position: relative; top: 0px; padding: 15px; color: #000; width: 95%;'>
                        <form id='superherofm_stripe' method='post' onsubmit='return false;'>

                            <div class="input-group">
                                <span class="input-group-addon input-group-cc">
	                            	<i class='glyphicon glyphicon-ok'></i>
	                            </span>
                                <input required type="text" class="form-control" data-stripe="number" placeholder="Credit Card #">
                            </div>

                            <div class="input-group" style='margin-top: 10px; width: 48%;'>
                                <span class="input-group-addon">
	                            	<i class='glyphicon glyphicon-ok'></i>
	                            </span>
                                <input required type="text" class="form-control" data-stripe="exp-month" placeholder="MM" style='width:50px;'>
                                <input required type="text" class="form-control" data-stripe="exp-year" placeholder="YY" style='width:50px;'>
                            </div>

                            <div class="input-group" style='margin-top: 10px; width: 30%;'>
                                <span class="input-group-addon">
	                            	<i class='glyphicon glyphicon-ok'></i>
	                            </span>
                                <input required type="text" class="form-control" data-stripe="cvc" placeholder="CVC" style='width:60px;'>
                            </div>

                            <div class="input-group" style='margin-top: 10px;'>
                                <span class="input-group-addon">
						  			<span class='glyphicon glyphicon-ok'></span>
                                </span>
                                <input required type="email" class="form-control" data-stripe="email" placeholder="Email address">
                            </div>

                            <input type='hidden' id='plan' value='' />
                            <button class='btn btn-blue' id="cc_submit" type="submit" style='margin-left: 0px; margin-top: 10px;'>Subscribe now</button>
                        </form>

                    </div>

                </div>



            </div>
        </div>

    </div>
